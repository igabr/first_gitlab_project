terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.13.0"
    }
  }
}

provider "aws" {
  region = "us-west-1"
}
variable "vpc" {
  type        = map(any)
  description = "list of subnets"
  default = {
    vpc_1 = "10.0.0.0/16",
    vpc_2 = "10.1.0.0/16",
  }
}

resource "aws_vpc" "main" {
  for_each   = var.vpc
  cidr_block = each.value
  tags = {
    "name" = each.key
  }
}